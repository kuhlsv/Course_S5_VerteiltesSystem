﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using VS_Models;

namespace Service1_WordSort
{
    [ServiceContract]
    public interface IService1WordSort
    {
        [OperationContract(Name = "Main")]
        string SortWordsDistributed(string text);

        [OperationContract]
        List<string> SortWords(string text);
    }

    public class Service1WordSort : IService1WordSort
    {
        const int MIN_LENGTH = 1000;
        string serviceName = "Service1_WordSort.dll"; // This will not effect the name of the service. The service will be generated in Project Node in Program.cs!
        HeartbeatSingleton hbs = HeartbeatSingleton.Instance;

        public string SortWordsDistributed(string text)
        {
            // Get a list with all nodes which includes this service
            List<Service> serviceOfotherNodes = new List<Service>();
            foreach (Node node in hbs.OwnNodeList.GetList)
            {
                foreach (Service service in node.GetServices())
                {
                    if (node.State == Node.NodeStates.Online && service.Name.ToLower() == serviceName.ToLower())
                    {
                        serviceOfotherNodes.Add(service);
                    }
                }
            }

            List<string> splitedString = SplitStringInParts(text, serviceOfotherNodes.Count(), MIN_LENGTH);
            List<List<string>> sortWordListList = new List<List<string>>();
            Thread.Sleep(1000);
            Task<List<List<string>>> sendToServiceTask = SendToServices(splitedString, serviceOfotherNodes);
            sendToServiceTask.Wait();
            sortWordListList = sendToServiceTask.Result;

            List<string> sortWords = sortWordListList[0];
            sortWordListList.RemoveAt(0);

            while (sortWordListList.Count() > 0)
            {
                int insertIndex = 0;
                foreach (string word in sortWordListList[0])
                {
                    bool workedOffWord = false;
                    for (; !workedOffWord && insertIndex < sortWords.Count; insertIndex++)
                    {
                        if (String.Compare(word, sortWords[insertIndex]) < 0)
                        {
                            sortWords.Insert(insertIndex, word);
                            workedOffWord = true;
                        }
                        else if (String.Compare(word, sortWords[insertIndex]) == 0)
                        {
                            // If same words, don't insert an continue with next word.
                            workedOffWord = true;
                        }
                    }
                    if (!workedOffWord)
                    {
                        sortWords.Add(word);
                    }
                }
                sortWordListList.RemoveAt(0);
            }
            string tempResult = String.Join(", ", sortWords.ToArray());
            return tempResult;
        }

        async Task<List<List<string>>> SendToServices(List<string> splitedString, List<Service> services)
        {
            List<List<string>> sortWordListList;
            if (splitedString.Count() > 1)
            {
                List<string> tempSplitedString = new List<string>(splitedString);
                tempSplitedString.RemoveAt(0);
                List<Service> tempServices = new List<Service>(services);
                tempServices.RemoveAt(0);
                Task<List<List<string>>> getSortWordsListList = SendToServices(tempSplitedString, tempServices);

                List<string> tempSortWords = null;
                EndpointAddress ep = new EndpointAddress(services[0].Uri);
                ChannelFactory<IService1WordSort> cFac = new ChannelFactory<IService1WordSort>(new BasicHttpBinding(), ep);
                IService1WordSort proxy = null;
                try
                {
                    // Try to send List
                    proxy = cFac.CreateChannel();
                    tempSortWords = proxy.SortWords(splitedString[0]);
                }
                catch (Exception ex)
                {
                }
                sortWordListList = await getSortWordsListList;
                if (tempSortWords != null)
                {
                    sortWordListList.Add(tempSortWords);
                }
            }
            else
            {
                List<string> tempSortWords = new List<string>();
                EndpointAddress ep = new EndpointAddress(services[0].Uri);
                ChannelFactory<IService1WordSort> cFac = new ChannelFactory<IService1WordSort>(new BasicHttpBinding(), ep);
                IService1WordSort proxy = null;
                try
                {
                    // Try to send List
                    proxy = cFac.CreateChannel();
                    tempSortWords = proxy.SortWords(splitedString[0]);
                }
                catch (Exception ex)
                {
                }
                sortWordListList = new List<List<string>>();
                if (tempSortWords != null)
                {
                    sortWordListList.Add(tempSortWords);
                }
            }
            return sortWordListList;
        }

        public List<string> SortWords(string text)
        {
            Log.F(this, HeartbeatSingleton.Instance.OwnNodeDummy.IP + ":" + HeartbeatSingleton.Instance.OwnNodeDummy.Port + " " + text);
            Regex rgx = new Regex("[^a-zA-Zäöüß0-9 -]");
            text = rgx.Replace(text, " ");
            List<string> words = text.Split(' ').ToList();
            words.Sort();
            words = words.Distinct().ToList();
            return words;
        }

        private List<string> SplitStringInParts(string text, int amoundOfParts, int minLength)
        {
            List<string> result = new List<string>();

            int substringLength = text.Length / amoundOfParts;

            // Calculate amounds of parts so that substringLength is at least 1000.
            for (int i = 0; substringLength <= minLength && (amoundOfParts - i) > 0; i++)
            {
                substringLength = text.Length / (amoundOfParts - i);
            }

            while (text.Length > 0)
            {
                int splitIndex = substringLength < text.Length ? substringLength : text.Length;
                // Checking for next whitespace.
                while (splitIndex < text.Length && text[splitIndex] != ' ')
                {
                    splitIndex++;
                }

                result.Add(text.Substring(0, splitIndex));
                text = text.Substring(splitIndex, text.Length - splitIndex);
            }

            return result;
        }
    }
}
