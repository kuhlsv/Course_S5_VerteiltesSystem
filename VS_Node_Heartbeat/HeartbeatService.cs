﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using VS_Models;

namespace VS_Node_Heartbeat
{
    public class HeartbeatService : IHeartbeatService
    {
        HeartbeatSingleton hbs = HeartbeatSingleton.Instance;

        public NodeList GetNodeList()
        {
            return hbs.OwnNodeList;
        }

        public void ReceiveNodeList(NodeList nodeList)
        {
            if (hbs.OwnNodeList.CompareAndAdoptChanges(nodeList, new PingDelegate(TryToPingNode), hbs.TimeToLive))
            {
                hbs.FindeAndSetFollowingNode();
            }
        }

        public bool ReturnPing()
        {
            return true;
        }

        private bool TryToPingNode(Node node)
        {
            EndpointAddress ep = new EndpointAddress("http://" + node.IP + ":" + node.Port + "/heartbeat");
            ChannelFactory<IHeartbeatService> cFac = new ChannelFactory<IHeartbeatService>(new BasicHttpBinding(), ep);
            IHeartbeatService proxy = null;
            try
            {
                proxy = cFac.CreateChannel();
                proxy.ReturnPing();
                return true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex);
                return false;
            }
        }
    }
}
