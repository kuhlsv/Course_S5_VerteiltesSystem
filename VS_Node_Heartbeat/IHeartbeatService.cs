﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using VS_Models;

namespace VS_Node_Heartbeat
{
    [ServiceContract]
    public interface IHeartbeatService
    {
        [OperationContract]
        void ReceiveNodeList([MessageParameter(Name = "NodeListe")] NodeList nodeList);

        [OperationContract(Name = "Main")]
        NodeList GetNodeList();

        [OperationContract]
        bool ReturnPing();
    }
}
