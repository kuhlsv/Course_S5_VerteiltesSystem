﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS_Models
{
    public class HeartbeatSingleton
    {
        private static HeartbeatSingleton instance = null;
        private static readonly object padlock = new object();

        private static NodeList ownNodeList;
        private static Node followingNode;
        private static int followingNodeIndex;
        private static int ownNodeIndex;
        private static Node ownNode;
        private static int timeToLive;
        private static int heartbeatIntervall;

        public int TimeToLive { get => timeToLive; }
        public int HeartbeatIntervall { get => heartbeatIntervall; }

        public NodeList OwnNodeList { get => ownNodeList; set => ownNodeList = value; }
        public Node FollowingNode { get => followingNode; set => followingNode = value; }
        public int FollowingNodeIndex { get => followingNodeIndex; set => followingNodeIndex = value; }
        // OwnNodeDummy only for Heartbeat / NodeList uses. To get the real node that represents this operating node use this.Get(OwnNodeIndex)
        public Node OwnNodeDummy { get => ownNode; set => ownNode = value; }
        public int OwnNodeIndex { get => ownNodeIndex; set => ownNodeIndex = value; }

        private HeartbeatSingleton()
        {
        }

        public static HeartbeatSingleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new HeartbeatSingleton();
                        ownNodeList = new NodeList();
                        followingNode = new Node();
                        ownNode = new Node();
                        // Set default values
                        timeToLive = 20000;
                        heartbeatIntervall = 20000;
                        try
                        {
                            Config config = new Configuration().Parse();
                            timeToLive = (int)config.HeartbeatSettings.Timeout;
                            heartbeatIntervall = (int)config.HeartbeatSettings.Interval;
                        } catch (Exception e)
                        {
                            Console.WriteLine("Can not read config.\n" + e.Message);
                        }
                    }
                    return instance;
                }
            }
        }

        public void FindeAndSetFollowingNode()
        {
            // If ownNode is not on the same index, look for ownNodeIndex
            if (this.OwnNodeIndex < this.OwnNodeList.Count() && !(this.OwnNodeList.GetNode(this.OwnNodeIndex).IsSameNode(this.OwnNodeDummy)) || this.OwnNodeIndex >= this.OwnNodeList.Count())
            {
                bool foundNode = false;
                for (int i = 0; i < this.OwnNodeList.Count() && !foundNode; i++)
                {
                    if (this.OwnNodeList.GetNode(i).IsSameNode(this.OwnNodeDummy))
                    {
                        foundNode = true;
                        this.OwnNodeIndex = i;
                    }
                }
            }

            // finde and set followingNode
            bool foundFollowingNode = false;
            for(int i = this.OwnNodeIndex + 1; !foundFollowingNode; i++)
            {
                if(i >= this.OwnNodeList.Count())
                {
                    i = 0;
                }
                if(this.OwnNodeList.GetNode(i).State == Node.NodeStates.Online)
                {
                    foundFollowingNode = true;
                    this.FollowingNode = this.OwnNodeList.GetNode(i);
                    this.FollowingNodeIndex = i;
                }
            }
        }
    }
}
