# VerteiltesSystem

[Info] <br>
Study procet for the module verteilte systeme <br>

[Version] <br>
.NET 4.6.1 <br>

[Struktur] <br>
VS_Client -> WPF Anwendung für integrierten WCF <br>
VS_Webserver -> Konsolen-App für integrierten WCF <br>
VS_Node -> Konsolen-App für integrierten WCF <br>
VS_Node_Heartbeat -> Der Heartbeat service für den austausch der Listen <br>
VS_Node_UpdateService -> Service 1 für den Upload <br>
VS_Node_ClientRouter -> Router-Service zum weiterleiten von Aufrufen und das verteilen <br>
VS_Models -> Verteilte Modles um die Entwicklung zu vereinfachen <br>
ServiceN -> WCF Dienst für die Nodes für jeweils eine Aufgabe <br>

[Debug] <br>
Datenbankpakete laden in der NuGet Konsole <br>
Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r<br>

"Cannot attach file ... to Database"<br>
Open the Package Manager Console. Run these commands:<br>
sqllocaldb stop MSSQLLocalDB<br>
sqllocaldb delete MSSQLLocalDB<br>

[Konventionen]<br>
Dll -> ServiceContractInterface erst definieren anschließend die Klasse die das Interface verwendet. Klasse muss eine EinstiegsMethode haben welche an erster Stelle steht