﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;

namespace Service2_Calculator
{
    [ServiceContract]
    public interface IService2Calculator
    {
        [OperationContract(Name = "Main")]
        string SortWordsDistributed(string calculation);
    }

    public class Service2Calculator : IService2Calculator
    {
        public string SortWordsDistributed(string calculation)
        {
            string result;
            calculation = calculation.Replace(" ", "");
            if (Regex.Match(calculation, @"^\d+([+\-\*\/]\d+)*$", RegexOptions.IgnoreCase).Success)
            {
                try
                {
                    // OperationLeve1 => * /
                    // OperationLevel2 => + -
                    string[] operationLevel1dArray = calculation.Split(new char[] { '+', '-' });
                    Regex pattern = new Regex(@"[\d\*\/]");
                    string operationLevel2Operators = pattern.Replace(calculation, "");

                    List<double> operationLevel1Results = new List<double>();
                    foreach (string operation in operationLevel1dArray)
                    {
                        string[] numbers = operation.Split(new char[] { '*', '/' });
                        Regex pattern2 = new Regex(@"\d");
                        string operationLevel1Operators = pattern2.Replace(operation, "");
                        operationLevel1Results.Add(Int32.Parse(numbers[0]));
                        for (int i = 0; i < operationLevel1Operators.Length; i++)
                        {
                            if (operationLevel1Operators[i] == '*')
                            {
                                operationLevel1Results[operationLevel1Results.Count - 1] *= Int32.Parse(numbers[i + 1]);
                            }
                            else
                            {
                                operationLevel1Results[operationLevel1Results.Count - 1] /= Int32.Parse(numbers[i + 1]);
                            }
                        }
                    }

                    double operationLevel2Results = operationLevel1Results[0];
                    for (int i = 0; i < operationLevel2Operators.Length; i++)
                    {
                        if (operationLevel2Operators[i] == '+')
                        {
                            operationLevel2Results += operationLevel1Results[i + 1];
                        }
                        else
                        {
                            operationLevel2Results -= operationLevel1Results[i + 1];
                        }
                    }

                    result = calculation + " = " + operationLevel2Results.ToString();
                }
                catch (DivideByZeroException dbze)
                {
                    result = "Division durch 0 ist nicht möglich.";
                }
                catch (Exception e)
                {
                    result = "Rechnung ist nicht valide.";
                }
            }
            else
            {
                result = "Keine valide Rechnung. Nur ganze positive Zahlen und die Operatoren + - * / sind erlaubt.";
            }
            return result;
        }
    }
}
