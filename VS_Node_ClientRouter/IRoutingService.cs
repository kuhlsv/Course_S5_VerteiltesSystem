﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using VS_Models;

namespace VS_Node_ClientRouter
{
    [ServiceContract]
    public interface IRoutingService
    {
        [OperationContract]
        List<Service> GetServices();

        [OperationContract]
        Object RunService(Service service, object[] userArgs);
    }


}
