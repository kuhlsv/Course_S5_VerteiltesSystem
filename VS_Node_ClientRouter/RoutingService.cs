﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using VS_Models;

namespace VS_Node_ClientRouter
{
    public class RoutingService : IRoutingService
    {
        HeartbeatSingleton hbs = HeartbeatSingleton.Instance;
        private Object serviceContractProxy;
        private const string START_METHOD_NAME = "Main";

        public List<Service> GetServices()
        {
            List<Service> services = new List<Service>();
            foreach (Node node in hbs.OwnNodeList.GetList)
            {
                List<Service> nodeServices = node.Services;
                foreach (Service service in nodeServices)
                {
                    // Check four double items
                    if (node.State == Node.NodeStates.Online
                        && services.Find(x => x.Name == service.Name) == null)
                    {
                        services.Add(service);
                    }
                }
            }
            return services;
        }

        public Object RunService(Service service, object[] userArgs)
        {
            List<Service> services = GetServices();
            Service choosenService = services.Find(x => x.Name == service.Name);
            Object result = null;
            if (choosenService != null)
            {
                if (OpenConnection(choosenService))
                {
                    result = Run(userArgs);
                }
            }
            CloseConnection();
            return result;
        }

        private Object Run(object[] userArgs)
        {
            // Run method with user inputs
            object retVal = serviceContractProxy.GetType().GetMethod(START_METHOD_NAME).Invoke(serviceContractProxy, userArgs);
            Console.WriteLine("Result from Node: " + retVal.ToString());
            return retVal;
        }

        private bool OpenConnection(Service service)
        {
            Boolean valid = false;
            // Get Metadata
            try
            {
                Uri mexAddress = new Uri(service.Uri, "?wsdl");
                MetadataExchangeClientMode mexMode = MetadataExchangeClientMode.HttpGet;
                MetadataExchangeClient mexClient = new MetadataExchangeClient(mexAddress, mexMode);
                mexClient.ResolveMetadataReferences = true;
                MetadataSet metaSet = mexClient.GetMetadata();
                // Parse Metadata
                WsdlImporter importer = new WsdlImporter(metaSet);
                // Take first. This requires that all services have just one contract
                ContractDescription contract = importer.ImportAllContracts().First();
                ServiceEndpoint endpoint = importer.ImportAllEndpoints().First();
                // Generate contract
                ServiceContractGenerator generator = new ServiceContractGenerator();
                generator.GenerateServiceContractType(contract);
                CodeDomProvider codeDomProvider = CodeDomProvider.CreateProvider("C#");
                // Get them dynamicly -> Not possible
                CompilerParameters compilerParameters = new CompilerParameters(new string[] { "System.dll", "System.ServiceModel.dll", "System.Runtime.Serialization.dll" });
                compilerParameters.GenerateInMemory = true;
                // Generate Contract Code
                CompilerResults results = codeDomProvider.CompileAssemblyFromDom(compilerParameters, generator.TargetCompileUnit);
                if (results.Errors.Count > 0)
                {
                    throw new Exception("There were errors during generated code compilation");
                }
                else
                {
                    // Generate wcf client-proxy instance from result code
                    Type clientProxyType = results.CompiledAssembly.GetTypes().First(
                        t => t.IsClass && t.GetInterface(contract.Name) != null && t.GetInterface(typeof(ICommunicationObject).Name) != null);
                    serviceContractProxy = results.CompiledAssembly.CreateInstance(
                        clientProxyType.Name,
                        false,
                        System.Reflection.BindingFlags.CreateInstance,
                        null,
                        new object[] { endpoint.Binding, endpoint.Address },
                        CultureInfo.CurrentCulture,
                        null
                    );
                    valid = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                valid = false;
            }
            return valid;
        }

        private Boolean CloseConnection()
        {
            serviceContractProxy = null;
            return true;
        }
    }
}
