﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace VS_Node_UpdateService
{
    [ServiceContract]
    public interface IUpdate
    {
        [OperationContract]
        void Upload(RemoteFileInfo request);

        [OperationContract]
        void Delete(string name);

        [OperationContract]
        void SetState(VS_Models.Node.NodeStates NewState);

    }
    [MessageContract]
    public class RemoteFileInfo : IDisposable
    {
        [MessageHeader(MustUnderstand = true)]
        public string FileName;

        [MessageBodyMember]
        public System.IO.Stream FileByteStream;

        public void Dispose()
        {
            if (FileByteStream != null)
            {
                FileByteStream.Close();
                FileByteStream = null;
            }
        }
    }
}
