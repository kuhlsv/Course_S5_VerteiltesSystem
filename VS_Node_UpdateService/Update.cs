﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

namespace VS_Node_UpdateService
{
    public class Update : IUpdate
    {
        string UploadFolder = Environment.CurrentDirectory + @"\Upload\";

        public void Delete(string name)
        {   //send event to close the service and remove it from list
            CustomEventArgs eventArgs = new CustomEventArgs();
            eventArgs.Message = name;
            SendCloseEvent(eventArgs);
            //delete dll after host is closed
            string filePath = Path.Combine(UploadFolder, name);
            File.Delete(filePath);
        }

        public void Upload(RemoteFileInfo request)
        {
            FileStream targetStream = null;
            Stream sourceStream = request.FileByteStream;
            string filePath = Path.Combine(UploadFolder, request.FileName);

            //send event to close the host in case there is an older version 
            CustomEventArgs eventArgs = new CustomEventArgs();
            eventArgs.Message = request.FileName;
            SendCloseEvent(eventArgs);

            //download the dll into the filesystem
            using (targetStream = new FileStream(filePath, FileMode.Create,
                                  FileAccess.Write, FileShare.None))
            {
                //read from the input stream in 65000 byte chunks

                const int bufferLen = 65000;
                byte[] buffer = new byte[bufferLen];
                int count = 0;
                while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                {
                    // save to output stream
                    targetStream.Write(buffer, 0, count);
                }
                targetStream.Close();
                sourceStream.Close();

                //let the Node know to start the new Service
                SendStartEvent(eventArgs);
            }
        }

        public void SetState(VS_Models.Node.NodeStates NewState)
        {
            //send event to change the state frome the current Node
            CustomEventArgs customEventArgs = new CustomEventArgs();
            customEventArgs.nodeStates = NewState;
            SendStateEvent(customEventArgs);
        }

        // create events
        public static event EventHandler<CustomEventArgs> CloseHostEvent;
        public static event EventHandler<CustomEventArgs> StartHostEvent;
        public static event EventHandler<CustomEventArgs> SetStateEvent;

        //Acitvate Events
        public void SendCloseEvent(CustomEventArgs eventArgs)
        {
            if(CloseHostEvent != null)
            CloseHostEvent(null, eventArgs);
        }
        public void SendStartEvent(CustomEventArgs eventArgs)
        {
            if (StartHostEvent != null)
                StartHostEvent(null, eventArgs);
        }
        public void SendStateEvent(CustomEventArgs eventArgs) {
            if (SetStateEvent != null)
                SetStateEvent(null, eventArgs);
        }
        // ggf auslagern
        public class CustomEventArgs : EventArgs
        {
            public string Message;
            public string CallingMethod;
            public VS_Models.Node.NodeStates nodeStates; 
        }
    }
}
