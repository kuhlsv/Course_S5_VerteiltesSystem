﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using VS_Models;

namespace VS_Client.Viewmodel
{
    class ConnectionViewModel
    {
        private Connection connection;

        public ConnectionViewModel()
        {
            // Initialisation
            Connection = new Connection();
        }

        public Connection Connection { set => connection = value; get => connection; }

        public string Address
        {
            get
            {
                return connection.Address;
            }
            set
            {
                if (!IPAddress.TryParse(value, out IPAddress buffer))
                {
                    connection.Address = Connection.DEFAULT_IP;
                    Console.WriteLine("Error: Can not parse input to a IP-Address! Using default.\n");
                }
                else
                {
                    connection.Address = value;
                }
            }
        }

        public int Port { get=> connection.Port; set => connection.Port = value; }

        public List<Service> GetServices()
        {
            EndpointAddress ep = new EndpointAddress("http://" + Address + ":" + Port + "/clientRouter");
            ChannelFactory<VS_Node_ClientRouter.IRoutingService> cFac = new ChannelFactory<VS_Node_ClientRouter.IRoutingService>(new BasicHttpBinding(), ep);
            VS_Node_ClientRouter.IRoutingService proxy;
            List<Service> services = new List<Service>();
            try
            {
                proxy = cFac.CreateChannel();
                services = proxy.GetServices();
            }
            catch (Exception ex)
            {
                services = null;
                Console.WriteLine(ex);
            }
            return services;
        }

        public object RunService(Service service, Object[] values)
        {
            EndpointAddress ep = new EndpointAddress("http://" + Address + ":" + Port + "/clientRouter");
            ChannelFactory<VS_Node_ClientRouter.IRoutingService> cFac = new ChannelFactory<VS_Node_ClientRouter.IRoutingService>(new BasicHttpBinding(), ep);
            VS_Node_ClientRouter.IRoutingService proxy;
            Object result = new Object();
            try
            {
                proxy = cFac.CreateChannel();
                result = proxy.RunService(service, values);
            }
            catch (Exception ex)
            {
                result = null;
                Console.WriteLine(ex);
            }
            return result;
        }
    }
}
