﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VS_Client.Model;
using VS_Models;

namespace VS_Client.Viewmodel
{
    class ServicesViewModel : ObservableCollection<Service>
    {
        private int selectedService;

        public ServicesViewModel()
        {
            // Initialisation
            this.SelectedService = -1;
        }

        /// <summary> 
        /// Public attribute to set selected service from UI.
        /// </summary>
        public int SelectedService
        {
            get
            {
                return selectedService;
            }
            set
            {
                selectedService = value;
            }
        }
    }
}
