﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit;
using VS_Models;
using VS_Client.Viewmodel;
using System.Reflection;
using System.Threading;

/// <summary>
/// To Add a new DataType-Input like "Image" it is important to change two methods
/// GenerateInputFields() and getServiceInfoFromInput()
/// </summary>
namespace VS_Client
{
    /// <summary>
    /// Interaktionslogik für InputWindow.xaml
    /// </summary>
    public partial class InputWindow : Window
    {
        private const int ITEM_HEIGHT = 25;
        private const int ITEM_WIDTH = 350;
        private const int GRID_HEIGHT = 40;
        private List<Control> controlElements;
        public delegate void RunService(Object[] results);
        public RunService RunInMain;

        public InputWindow(Service service)
        {
            InitializeComponent();
            controlElements = new List<Control>();
            SizeToContent = SizeToContent.Height;
            Title = service.Name != null && service.Name != "" ? service.Name : "Run";
            GenerateInputFields(service);
        }
        
        private void GenerateInputFields(Service service)
        {
            var cvm = FindResource("cvm") as ConnectionViewModel;
            List<Parameter> parameters = service.Parameters;
            if (parameters.Count > 0)
            {
                for (int i = 0, j = 1; i < parameters.Count; i++, j++)
                {
                    // Extend grid
                    RowDefinition row = new RowDefinition();
                    row.Height = new GridLength(GRID_HEIGHT);
                    grid.RowDefinitions.Add(row);
                    // Add label
                    Label label = new Label();
                    label.Content = parameters[i].Name;
                    label.Height = ITEM_HEIGHT;
                    label.Width = GRID_HEIGHT*4;
                    grid.Children.Add(label);
                    Grid.SetColumn(label, 0);
                    Grid.SetRow(label, j);
                    // Generate input
                    switch (Type.GetTypeCode(parameters[i].Type))
                    {
                        case TypeCode.Int32:
                        case TypeCode.Int16:
                        case TypeCode.Int64:
                            DecimalUpDown iDud = new DecimalUpDown();
                            iDud.Text = parameters[i].Type.ToString();
                            iDud.FormatString = "n";
                            iDud.Value = 0;
                            iDud.Increment = 1;
                            addValues(iDud, parameters[i], j);
                            break;
                        case TypeCode.Double:
                            DecimalUpDown dDud = new DecimalUpDown();
                            dDud.Text = parameters[i].Type.ToString();
                            dDud.Value = 0.00M;
                            dDud.Increment = 0.01M;
                            dDud.FormatString = "f";
                            addValues(dDud, parameters[i], j);
                            break;
                        case TypeCode.Boolean:
                            CheckBox bCb = new CheckBox();
                            addValues(bCb, parameters[i], j);
                            break;
                        case TypeCode.Char:
                            TextBox cBox = new TextBox();
                            cBox.Text = parameters[i].Type.ToString();
                            cBox.GotFocus += TextBox_GotFocus;
                            cBox.LostFocus += TextBox_LostFocus;
                            cBox.MaxLength = 1;
                            addValues(cBox, parameters[i], j);
                            break;
                        case TypeCode.String:
                        default:
                            TextBox sBox = new TextBox();
                            sBox.Text = parameters[i].Type.ToString();
                            sBox.GotFocus += TextBox_GotFocus;
                            sBox.LostFocus += TextBox_LostFocus;
                            addValues(sBox, parameters[i], j);
                            break;
                    }
                }
            }
            else
            {
                // Else run direktly
                Run_Click(null, null);
            }
        }

        private void addValues(Control input, Parameter para, int row)
        {
            // Add input
            input.Name = para.Name;
            input.ToolTip = para.Type;
            input.Height = ITEM_HEIGHT;
            input.Width = ITEM_WIDTH;
            try
            {
                // Add WPF control
                grid.Children.Add(input);
                Grid.SetColumn(input, 1);
                Grid.SetRow(input, row);
            }catch(Exception buffer)
            {
                try
                {
                    // Add xceed control
                    this.AddChild(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine(buffer.Message, e.Message);
                }
            }
            // Add to list for parameters
            controlElements.Add(input);
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (string.IsNullOrWhiteSpace(tb.Text))
            {
                tb.Text = tb.ToolTip.ToString();
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if(tb.Text == tb.ToolTip.ToString())
            {
                tb.Text = "";
            }
        }

        private void Run_Click(object sender, RoutedEventArgs e)
        {
            var svm = FindResource("svm") as ServicesViewModel;
            object[] data = GetFormInputAsObject(svm[svm.SelectedService]);
            // Run Service in new thread
            this.Hide();
            RunInMain(data);
            // Cleanup
            Close();
        }

        private Object[] GetFormInputAsObject(Service service)
        {
            // Get types from methods
            List<Parameter> parameters = service.Parameters;
            List<Object> values = new List<Object>();
            if (parameters.Count > 0)
            {
                for (int i = 0; i < controlElements.Count; i++)
                {
                    string val = GetValuesAsObject(controlElements[i]);
                    switch (Type.GetTypeCode(parameters[i].Type))
                    {
                        case TypeCode.Int32:
                            int int32 = 0;
                            Int32.TryParse(val, out int32);
                            values.Add(int32);
                            break;
                        case TypeCode.Int16:
                            short int16 = 0;
                            short.TryParse(val, out int16);
                            values.Add(int16);
                            break;
                        case TypeCode.Int64:
                            long int64 = 0;
                            long.TryParse(val, out int64);
                            values.Add(int64);
                            break;
                        case TypeCode.Double:
                            double doub = 0;
                            double.TryParse(val, out doub);
                            values.Add(doub);
                            break;
                        case TypeCode.Boolean:
                            bool boolean = false;
                            bool.TryParse(val, out boolean);
                            values.Add(boolean);
                            break;
                        case TypeCode.Char:
                            char c = char.MinValue;
                            char.TryParse(val, out c);
                            values.Add(c);
                            break;
                        case TypeCode.String:
                        default:
                            string s = (val).ToString();
                            values.Add(s);
                            break;
                    }
                }
            }
            return values.ToArray();
        }

        private string GetValuesAsObject(Control control)
        {
            string buffer = "";
            // Use reflection to get value from unknown control type
            PropertyInfo piV = control.GetType().GetProperty("Value");
            if (piV != null)
            {
                var value = (piV.GetValue(control, null));
                buffer = value.ToString();
                Console.WriteLine("Read from input -> " + value);
            }
            else
            {
                PropertyInfo piT = control.GetType().GetProperty("Text");
                if (piT != null)
                {
                    var value = (piT.GetValue(control, null));
                    buffer = value.ToString();
                    Console.WriteLine("Read from input -> " + value);
                }
                else
                {
                    string empty = "";
                    buffer = empty;
                }
            }
            return buffer;
        }
    }
}
