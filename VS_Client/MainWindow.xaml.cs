﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Threading;
using VS_Client.Viewmodel;
using VS_Models;
using System.Net;
using System.Collections.Concurrent;

namespace VS_Client
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum ProgressState
        {
            Online,
            Offline,
            Processing
        }

        List<Service> services; // Services of the view list
        BlockingCollection<Thread> RunningThreads;

        public MainWindow()
        {
            InitializeComponent();
            RunningThreads = new BlockingCollection<Thread>();
            // Progressbar
            progressbar.Minimum = 0;
            progressbar.Maximum = short.MaxValue;
            progressbar.Value = progressbar.Maximum;
            progressbar.Foreground = Brushes.Red;
            // Textbox
            addressBox.GotFocus += AddressBox_GotFocus;
            addressBox.LostFocus += AddressBox_LostFocus;
            portBox.GotFocus += PortBox_GotFocus;
            portBox.LostFocus += PortBox_LostFocus;
            resultBox.AppendText("\n");
        }

        public void SetProgressState(ProgressState state)
        {
            switch (state)
            {
                case ProgressState.Online:
                    progressbar.Foreground = Brushes.Green;
                    SetIndeterminate(false);
                    break;
                case ProgressState.Offline:
                    progressbar.Foreground = Brushes.Red;
                    SetIndeterminate(false);
                    break;
                case ProgressState.Processing:
                    progressbar.Foreground = Brushes.Orange;
                    SetIndeterminate(true);
                    break;
                default:
                    progressbar.Foreground = Brushes.Red;
                    SetIndeterminate(false);
                    break;
            }
        }

        private void SetIndeterminate(bool isIndeterminate)
        {
            progressbar.IsIndeterminate = isIndeterminate;
        }

        // Connect/Disconnect Button
        private async void Button_ClickAsync(object sender, RoutedEventArgs e)
        {
            SetProgressState(ProgressState.Processing);
            // Get ViewModels
            var svm = FindResource("svm") as ServicesViewModel;
            var cvm = FindResource("cvm") as ConnectionViewModel;
            if (connnectButton.Content.ToString() == "Connect")
            {
                // Set Connection
                if (!int.TryParse(portBox.Text, out int port)){
                    port = Connection.DEFAULT_PORT;
                    Console.WriteLine("Error: Can not parse input to a Port! Using default.\n");
                    WriteToBox("Error: Pls insert a valid Port! Try to use default Port.");
                }
                if (!IPAddress.TryParse(addressBox.Text, out IPAddress ip))
                {
                    IPAddress.TryParse(Connection.DEFAULT_IP, out ip);
                    Console.WriteLine("Error: Can not parse input to a IP Address! Using default.\n");
                    WriteToBox("Error: Pls insert a valid IP-Address! Try connecting to localhost.");
                }
                cvm.Connection = new Connection(ip.ToString(), port);
                services = new List<Service>();
                // Get Services
                await TakeServicesAsync(cvm);
                // Handle retunred services
                if (services == null)
                {
                    WriteToBox("Warning: Incorrect connection!");
                    SetProgressState(ProgressState.Offline);
                    return;
                }
                if(services.Count == 0)
                {
                    WriteToBox("Warning: No service found!");
                    connnectButton.Content = "Disconnect";
                    SetProgressState(ProgressState.Online);
                    return;
                }
                // Add Services
                foreach (Service service in services)
                {
                    // Update UI
                    svm.Add(service);
                }
                connnectButton.Content = "Disconnect";
                SetProgressState(ProgressState.Online);
            }
            else
            {
                connnectButton.Content = "Connect";
                SetProgressState(ProgressState.Offline);
                // Clear
                svm.Clear();
                cvm.Connection = new Connection();
                services = new List<Service>();
            }
        }

        // Get Services from ViewModel
        private async Task TakeServicesAsync(ConnectionViewModel con)
        {
            await Task.Run(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                // Get services from node
                List<Service> svc = con.GetServices();
                Thread.Sleep(1000);
                services = svc;
            });
            if (services == null)
            {
                WriteToBox("Error: Can not get services from called node!");
            }
        }

        // Get Run Service called from InputWindow
        public void RunServiceInThread(Object[] values)
        {
            // Run Service in new thread
            var svm = FindResource("svm") as ServicesViewModel;
            var cvm = FindResource("cvm") as ConnectionViewModel;
            Service service = svm[svm.SelectedService];
            Thread run = new Thread(new ThreadStart(() =>
            {
                // Run
                Object result = cvm.RunService(service, values);
                if (result == null)
                {
                    result = "No result!";
                }
                Dispatcher.Invoke(new Action(() =>
                {
                    ShowResult(svm[svm.SelectedService].Name, result);
                })); // Invoke to UI 
                RunningThreads.TryTake(out Thread buffer);
                buffer.Abort();
            }))
            {
                IsBackground = true
            };
            RunningThreads.Add(run);
            run.Start();
        }

        private void Button_StartService(object sender, RoutedEventArgs e)
        {
            var svm = FindResource("svm") as ServicesViewModel;
            if (svm.SelectedService >= 0)
            {
                try
                {
                    InputWindow inputWindow = new InputWindow(svm[svm.SelectedService])
                    {
                        RunInMain = RunServiceInThread
                    };
                    inputWindow.ShowDialog();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: Can not create View! May can not connect to get Metadata.");
                    Console.WriteLine(ex.Message);
                    WriteToBox("Error: Can not connect to the hosting node or get the service!");
                }

            }
        }

        public void ShowResult(string service, Object resObj)
        {
            try
            {
                this.resultBox.AppendText(service + ": " + resObj.ToString() + "\n");
                this.resultBox.ScrollToEnd();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }

        public void WriteToBox(string info)
        {
            try
            {
                this.resultBox.AppendText(info + "\n");
                this.resultBox.ScrollToEnd();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }
        
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Log.C(this, "Runnings threads count: " + RunningThreads.Count.ToString());
            WriteToBox("Info: Killed " + RunningThreads.Count.ToString() + " running services!");
            while (RunningThreads.Count != 0) {
                RunningThreads.TryTake(out Thread buffer);
                buffer.Abort();
            }
        }

        private void AddressBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(addressBox.Text))
            {
                addressBox.Text = Connection.DEFAULT_IP.ToString();
            }
        }

        private void AddressBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (addressBox.Text == Connection.DEFAULT_IP.ToString())
            {
                addressBox.Text = "";
            }
        }

        private void PortBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(portBox.Text))
            {
                portBox.Text = Connection.DEFAULT_PORT.ToString();
            }
        }

        private void PortBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (portBox.Text == Connection.DEFAULT_PORT.ToString())
            {
                portBox.Text = "";
            }
        }
    }
}
