﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VS_Models
{
    public delegate bool PingDelegate(Node node);

    [DataContract]
    public class NodeList
    {
        

        [DataMember]
        private long timestamp = 0;
        [DataMember]
        private bool hasOfflineNodes = false;
        [DataMember]
        private List<Node> listOfNodes;

        public long Timestamp { get => timestamp; set => timestamp = value; }
        public bool HasOfflineNodes { get => hasOfflineNodes; set => hasOfflineNodes = value; }
        public List<Node> GetList { get => listOfNodes; }

        public NodeList()
        {
            listOfNodes = new List<Node>();
            SetNewTimestamp();
        }

        public Node GetNode(int index)
        {
            try {
                return listOfNodes[index];
            }
            catch
            {
                throw;
            }
        }

        public bool ReplaceNode(Node node, int index)
        {
            return ReplaceNode(node, index, false);
        }

        public bool ReplaceNode(Node node, int index, bool setTimestamp)
        {
            if(index >= listOfNodes.Count)
            {
                return false;
            }
            else
            {
                listOfNodes[index] = node;
                if (setTimestamp)
                {
                    SetNewTimestamp();
                }
                return true;
            }
        }

        public int Count()
        {
            return listOfNodes.Count;
        }

        public List<Node> AddNodeAt(Node node, int index)
        {
            return AddNodeAt(node, index, false);
        }

        public List<Node> AddNodeAt(Node node, int index, bool setTimestamp)
        {
            if (index >= listOfNodes.Count)
            {
                AddNode(node);
            }
            else
            {
                listOfNodes.Insert(index, node);
            }
            if (setTimestamp)
            {
                SetNewTimestamp();
            }
            return listOfNodes;
        }

        public List<Node> AddNode(Node node)
        {
            return AddNode(node, false);
        }

        public List<Node> AddNode(Node node, bool setTimestamp)
        {
            listOfNodes.Add(node);
            if (setTimestamp)
            {
                SetNewTimestamp();
            }
            return listOfNodes;
        }

        public bool RemoveNodeAt(int index)
        {
            return RemoveNodeAt(index, false);
        }

        public bool RemoveNodeAt(int index, bool setTimestamp)
        {
            if (index < listOfNodes.Count)
            {
                listOfNodes.RemoveAt(index);
                if (setTimestamp)
                {
                    SetNewTimestamp();
                }
                //Console.WriteLine("NodeList Size: " + listOfNodes.Count);
                return true;
            }
            return false;
        }

        public bool RemoveNode(Node node)
        {
            return RemoveNode(node, false);
        }

        public bool RemoveNode(Node node, bool setTimestamp)
        {
            if (setTimestamp)
            {
                SetNewTimestamp();
            }
            return listOfNodes.Remove(node);
        }

        public bool SetNodeOnline(int index)
        {
            return SetNodeOnline(index, false);
        }

            public bool SetNodeOnline(int index, bool setTimestamp)
        {
            if (index < listOfNodes.Count)
            {
                listOfNodes[index].State = Node.NodeStates.Online;
                if (setTimestamp)
                {
                    SetNewTimestamp();
                }
                return true;
            }
            return false;
        }

        public bool SetNodeOffline(int index)
        {
            return SetNodeOffline(index, false);
        }

        public bool SetNodeOffline(int index, bool setTimestamp)
        {
            if (index < listOfNodes.Count)
            {
                listOfNodes[index].State = Node.NodeStates.Offline;
                listOfNodes[index].StateChangeTimestemp = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                this.HasOfflineNodes = true;
                if (setTimestamp)
                {
                    SetNewTimestamp();
                }
                return true;
            }
            return false;
        }

        public Node.NodeStates GetNodeState(int index)
        {
            return listOfNodes[index].State;
        }

        /*
         * Returns true if adopted changes. False if no Changes were made. 
         */

        public bool CompareAndAdoptChanges(NodeList givenNodeList, PingDelegate pingDelegate, int timeToLive)
        {
            bool helperListChanged = false;
            bool hasOfflineNodes = false;
            List<int> offlineNodeIndexCollectionList = new List<int>();
            
            // If givenNodeList has a newer timestamp or offline nodes
            if ( givenNodeList != null && (this.Timestamp < givenNodeList.Timestamp || givenNodeList.HasOfflineNodes))
            {
                helperListChanged = true;
                // Takes timestamp from givenNodeList. If this node will do some changes, timestamp will be updatet at that operation
                this.Timestamp = givenNodeList.Timestamp;

                /*
                 * Step 1: Adopt nodes from givenNodeList
                 */

                // Run through the list till the end of one
                for (int i = 0; i < this.Count() && i < givenNodeList.Count(); i++)
                {
                    // If both nodes are the same nodes
                    if (this.GetNode(i).IsSameNode(givenNodeList.GetNode(i)))
                    {
                        // Adopt node from givenNodeList
                        this.ReplaceNode(givenNodeList.GetNode(i), i);
                        // If Node is offline, collect index of this node
                        if(this.GetNode(i).State == Node.NodeStates.Offline)
                        {
                            hasOfflineNodes = true;
                        }
                    }
                    else
                    {
                        // If not same nodes, look for this node.
                        bool foundNode = false;
                        int searchIndex = i + 1;
                        bool enteredLoop = false;
                        for (; searchIndex < givenNodeList.Count(); searchIndex++)
                        {
                            enteredLoop = true;
                            foundNode = this.GetNode(i).IsSameNode(givenNodeList.GetNode(searchIndex)) ? true : foundNode;
                        }
                        // If loop is enterd at least one time, searchIndex is one greater than suposted to be because of post iteration
                        if (enteredLoop)
                        {
                            searchIndex--;
                        }
                        // If found node, insert all nodes between and adopt node
                        if (foundNode)
                        {
                             // First replace this node with the instance of this node from givenNodeList
                            this.ReplaceNode(givenNodeList.GetNode(searchIndex), i);
                            // Insert every node between backwords, so that order is the same
                            for (int insertIndex = searchIndex - 1; insertIndex >= i; insertIndex--)
                            {
                                this.AddNodeAt(givenNodeList.GetNode(insertIndex), i);
                                
                                // Get index of offline node which just added
                                if (this.GetNode(i).State == Node.NodeStates.Offline)
                                {
                                    hasOfflineNodes = true;
                                }
                            }
                        }
                        else
                        {
                            // If node is not in givenNodeList, delete this node
                            this.RemoveNodeAt(i);
                            // Decrement i because new node is on this index now and needs to be checked.
                            // old List | 1 |      new List | 1 |
                            //          | 2 |               | 2 |
                            //          | 3 |  <- remove    | 4 | <- needs to be checked, same index as old node
                            //          | 4 |               | 5 |
                            //          | 5 |               
                            i--;
                        }

                    }
                }

                // givenNodeList is bigger than ownList, than insert missing nodes from the end
                if (this.Count() < givenNodeList.Count())
                {
                    for (int i = this.Count(); i < givenNodeList.Count(); i++)
                    {
                        this.AddNode(givenNodeList.GetNode(i));
                    }
                }
                else if (this.Count() > givenNodeList.Count())
                {
                    // givenNodeList is smaller than ownList, than remove "overflow" nodes from ownList
                    for (int i = this.Count() - 1; i >= givenNodeList.Count(); i--)
                    {
                        this.RemoveNodeAt(i);
                    }
                }

                /*
                 * Step 2: Check offline declared Nodes
                 */

                if (hasOfflineNodes)
                {
                    hasOfflineNodes = false;
                    // Backward loop running, because if node is deletet, nodes with heigher index will get new index
                    for (int i = this.Count() - 1; i >= 0; i--)
                    {
                        if (pingDelegate(this.GetNode(i)))
                        {
                            // If can be pinged, set online and remove from offlineNodeIndexCollectionList
                            this.SetNodeOnline(i, true);
                        }
                        else
                        {
                            // If is still offline an timeToLive is over
                            if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - this.GetNode(i).StateChangeTimestemp > timeToLive)
                            {
                                this.RemoveNodeAt(i);
                            }
                            else
                            {

                                hasOfflineNodes = true;
                            }
                        }
                    }
                }
            }

            this.HasOfflineNodes = hasOfflineNodes;
            return helperListChanged;
        }

        public void SetNewTimestamp()
        {
            this.Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();
        }
    }
}
