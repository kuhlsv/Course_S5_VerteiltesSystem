﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace VS_Models
{
    [DataContract]
    public class Parameter
    {
        [DataMember]
        private string name;
        [DataMember]
        private string type;

        public string Name { get => name; set => name = value; }
        public Type Type { get => Type.GetType(type); set => type = value.FullName; }

        public Parameter()
        {
        }
    }
}
