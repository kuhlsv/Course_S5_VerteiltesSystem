using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace VS_Models
{
    [DataContract]
    public class Service
    {
        [DataMember]
        private string name;
        [DataMember]
        private Uri uri;
        [DataMember]
        private List<Parameter> parameters;
        [DataMember]
        private string returnType;

        public string Name { get => name; set => name = value; }
        public Uri Uri { get => uri; set => uri = value; }
        public List<Parameter> Parameters { get => parameters; set => parameters = value; }
        public Type ReturnType { get => Type.GetType(returnType); set => returnType = value.FullName; }

        public Service()
        {
            parameters = new List<Parameter>();
        }

        public void AddParameter(Parameter paramter) {
            this.Parameters.Add(paramter);
        }
    }
}