﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using System;
using System.IO;

namespace VS_Models
{
    /// <summary>
    /// Schema
    /// </summary>
    public partial class Config
    {
        [JsonProperty("heartbeat")]
        public HeartbeatSettings HeartbeatSettings { get; set; }

        [JsonProperty("client-connection")]
        public ConnectionSettings ConnectionSettings { get; set; }
    }

    public partial class ConnectionSettings
    {
        [JsonProperty("allocation")] // Manuel, Auto
        public string Allocation { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("port")]
        public long Port { get; set; }
    }

    public partial class HeartbeatSettings
    {
        [JsonProperty("timetolive")]
        public long Timeout { get; set; }

        [JsonProperty("interval")]
        public long Interval { get; set; }
    }

    /// <summary>
    /// Parser
    /// </summary>
    public class Configuration
    {
        private string DEV_DEFAULT_PATH = Directory.GetCurrentDirectory() + @"\..\..\..\VS_Models\Config.json";
        private string PROD_DEFAULT_PATH = Directory.GetCurrentDirectory() + @"\Config.json";
        private string path;

        public string Path { get => path; set => path = value; }

        public Configuration()
        {
            #if DEBUG
                Path = DEV_DEFAULT_PATH;
            #else
                Path = PROD_DEFAULT_PATH;
            #endif
        }

        public Configuration(string path)
        : base()
        {
            Path = path;
        }
        
        public Config Parse()
        { 
            string json = "";
            Config config = new Config();
            try
            {
                using (StreamReader r = new StreamReader(Path))
                {
                    json = r.ReadToEnd();
                }
            }catch(Exception e)
            {
                Console.WriteLine("-----\nError: Can not read config file! File: " + json + "\n-----");
                Console.WriteLine(e.Message);
            }
            try
            {
                config = JsonConvert.DeserializeObject<Config>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine("-----\nError: Can not parse config file! File: " + json + "\n-----");
                Console.WriteLine(e.Message);
            }
            return config;
        }
    }
}
