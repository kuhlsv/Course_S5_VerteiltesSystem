﻿using System;
using System.Collections.Generic;
using System.Net;

namespace VS_Models
{
    public class Connection
    {
        public const int DEFAULT_PORT = 8080;
        public const string DEFAULT_IP = "127.0.0.1";

        private IPAddress address;
        private int port;

        public string Address { get => address.ToString(); set => IPAddress.TryParse(value, out address); }
        public int Port { get => port; set => port = value; }

        public Connection(string address, int port)
        {
            this.Address = address;
            this.Port = port;
        }

        public Connection()
        {
            this.Address = DEFAULT_IP;
            this.Port = DEFAULT_PORT;
            try
            {
                Config conf = new Configuration().Parse();
                if (conf.ConnectionSettings.Allocation.ToLower() == "auto")
                {
                    if (conf.ConnectionSettings.Ip != "")
                    {
                        this.Address = conf.ConnectionSettings.Ip;
                    }
                    if (conf.ConnectionSettings.Port != 0)
                    {
                        int.TryParse(conf.ConnectionSettings.Port.ToString(), out this.port);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Can not read config.\n"+e.Message);
            }
        }

        public List<Service> GetServicesFromNode(List<Node> nodes, String IP, int Port)
        {
            List<Service> services = new List<Service>();
            foreach(Node node in nodes)
            {
                if (node.IP.ToString() == IP && node.Port == Port)
                {
                    Boolean checkPort = node.Port == Port;
                    services = node.GetServices();
                }
            }
            return services;
        }
    }
}
