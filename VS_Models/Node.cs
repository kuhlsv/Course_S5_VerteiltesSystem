﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;

namespace VS_Models
{
    [DataContract]
    public class Node
    {
        public enum NodeStates
        {
            Online,
            Offline,
            Maintenance,
            Error
        }
        [DataMember]
        private string name;
        [DataMember]
        private IPAddress ip;
        [DataMember]
        private int port;
        [DataMember]
        private List<Service> services;
        [DataMember]
        private NodeStates state;
        [DataMember]
        private long stateChangeTimestemp;

        public string Name { get => name; set => name = value; }
        public IPAddress IP { get => ip; set => ip = value; }
        public int Port { get => port; set => port = value; }
        public List<Service> Services { get => services; set => services = value; }
        public NodeStates State { get => state; set => state = value;}
        public long StateChangeTimestemp { get => stateChangeTimestemp; set => stateChangeTimestemp = value; }

        public Node()
        {
            services = new List<Service>();
        }

        public Node(string ipString, int port)
        {
            IPAddress.TryParse(ipString, out ip);
            this.Port = port;
        }

        public Node(IPAddress ip, int port)
        {
            this.IP = ip;
            this.Port = port;
        }

        public void AddService(Service service)
        {
            this.Services.Add(service);
        }

        public void RemoveService(Service service)
        {
            this.Services.Remove(service);
        }

        public Service GetService(int i)
        {
            return this.Services[i];
        }

        public List<Service> GetServices()
        {
            return this.services;
        }

        /*
         *  Checks if same Nodes based on the IP and Prot number
         */
        public bool IsSameNode(Node givenNode)
        {
            return (this.IP.Equals(givenNode.IP) && this.Port == givenNode.Port);
        }
    }
}
