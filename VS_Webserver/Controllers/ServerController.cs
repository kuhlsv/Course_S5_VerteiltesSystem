﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VS_Models;
using System.Net;
using System.ServiceModel;
using System.IO;
using VS_Node_UpdateService;

namespace VS_Webserver.Controllers
{
    [Authorize]
    public class ServerController : Controller
    {
        Connection con;

        // GET: Server

        //[HttpGet]
        public ActionResult Index(int? Port, String IP = null)
        {
            if (IP != null && Port != null)
            {
                con = new Connection
                {
                    Address = IP,
                    Port = (int)Port
                };
                ViewBag.nodes = GetNodes(IP, (int)Port);
                ViewBag.nodesExists = true;
            }
            else
            {
                ViewBag.nodes = new List<Node>() { };
            }


            return View();
        }

        

        public ActionResult List(String IP, int Port)
        {
            ViewBag.detailIP = IP;
            ViewBag.detailPort = Port;
            List<Service> services = GetServices(IP, Port);
            ViewBag.services = services;
            if (services.Count() > 0)
            {
                ViewBag.servicesExists = true;
            } else
            {
                ViewBag.sercicesExists = false;
            }
            
            return View();
        }

        public ActionResult SwitchMode(String IP, int Port, string NewState)
        {
            VS_Models.Node.NodeStates NewNewState = new VS_Models.Node.NodeStates();
            //SetState( Enum.TryParse<VS_Models.Node.NodeStates>(NewState, out NewNewState), IP, Port);
            Enum.TryParse<VS_Models.Node.NodeStates>(NewState, false, out NewNewState);
            SetState(NewNewState, IP, Port);
            //SetState(NewState, IP, Port);
            return RedirectToAction("Index", new { Port, IP});

        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, String IP, int Port)
        {

            try
            {
                if (file.ContentLength > 0)
                {
                    con = new Connection(IP, Port);
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("../uploads"), fileName);
                    file.SaveAs(path);
                    SendFile(file.InputStream, fileName, con);
                }
            } catch (Exception)
            {

            }

            return RedirectToAction("List", new {IP, Port});
        }

        public ActionResult Delete(String Name, String IP, int Port) {

            DeleteService(Name, IP, Port);

            return RedirectToAction("List", new { IP, Port});
        }

        public List<Node> GetNodes(String IP, int Port)
        {
            con = new Connection(IP, Port);
            EndpointAddress ep = new EndpointAddress("http://" + con.Address + ":" + con.Port + "/heartbeat");
            ChannelFactory<VS_Node_Heartbeat.IHeartbeatService> cFac = new ChannelFactory<VS_Node_Heartbeat.IHeartbeatService>(new BasicHttpBinding(), ep);
            VS_Node_Heartbeat.IHeartbeatService proxy;
            List<Node> nodes = new List<Node>();
            try
            {
                proxy = cFac.CreateChannel();
                nodes = proxy.GetNodeList().GetList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return nodes;
        }

        public List<Service> GetServices(String IP, int Port)
        {
            con = new Connection(IP, Port);
            return con.GetServicesFromNode(GetNodes(IP, Port), IP, Port);
        }
        
        public void SendFile(Stream stream, String filename, Connection con)
        {
            RemoteFileInfo remoteFileInfo = new RemoteFileInfo();
            remoteFileInfo.FileByteStream = stream;
            remoteFileInfo.FileName = filename;

            EndpointAddress ep = new EndpointAddress("http://" + con.Address + ":" + con.Port + "/update");
            ChannelFactory<VS_Node_UpdateService.IUpdate> cFac = new ChannelFactory<VS_Node_UpdateService.IUpdate>(new BasicHttpBinding(), ep);
            VS_Node_UpdateService.IUpdate proxy;
            List<Node> nodes = new List<Node>();
            try
            {
                proxy = cFac.CreateChannel();
                proxy.Upload(remoteFileInfo);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }            
        }

        public void DeleteService(string name, String IP, int Port)
        {

            con = new Connection(IP, Port);
            EndpointAddress ep = new EndpointAddress("http://" + con.Address + ":" + con.Port + "/update");
            ChannelFactory<VS_Node_UpdateService.IUpdate> cFac = new ChannelFactory<VS_Node_UpdateService.IUpdate>(new BasicHttpBinding(), ep);
            VS_Node_UpdateService.IUpdate proxy;
            try
            {
                proxy = cFac.CreateChannel();
                proxy.Delete(name);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void SetState(Node.NodeStates NewState, String IP, int Port)
        {
            con = new Connection(IP, Port);
            EndpointAddress ep = new EndpointAddress("http://" + con.Address + ":" + con.Port + "/update");
            ChannelFactory<VS_Node_UpdateService.IUpdate> cFac = new ChannelFactory<VS_Node_UpdateService.IUpdate>(new BasicHttpBinding(), ep);
            VS_Node_UpdateService.IUpdate proxy;
            List<Node> nodes = new List<Node>();
            try
            {
                proxy = cFac.CreateChannel();
                proxy.SetState(NewState);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}