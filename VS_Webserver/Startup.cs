﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VS_Webserver.Startup))]
namespace VS_Webserver
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
