﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Security.Policy;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VS_Models;
using VS_Node_ClientRouter;
using VS_Node_Heartbeat;
using VS_Node_UpdateService;

namespace VS_Node
{
    class Program
    {
        static HeartbeatSingleton hbs = HeartbeatSingleton.Instance;
        static ServiceHost heartbeatHost = null;
        static List<ServiceHost> userServiceHosts = new List<ServiceHost>();
        static ServiceHost routingHost = null;
        static string UploadFolder = Environment.CurrentDirectory + @"\Upload\";


        static void Main(string[] args)
        {
            Directory.CreateDirectory(UploadFolder);

            Heartbeat heartbeat = new Heartbeat();

            Node nodeToConnect = SetupNode();
            heartbeat.StartHeartbeat(nodeToConnect, hbs.HeartbeatIntervall);
            StartRouter();

            StartAllUserServices();
            
            // Host UpdateService
            ServiceHost hostUpdateService = null;
            try
            {
                hostUpdateService = new ServiceHost(typeof(Update));
                //Add Eventlistener to UpdateServiceHost
                Update.CloseHostEvent += HandleCloseEvent;
                Update.StartHostEvent += HandleStartEvent;
                Update.SetStateEvent += HandleStateEvent;
                hostUpdateService.AddServiceEndpoint(typeof(IUpdate),
                    new BasicHttpBinding(),
                    new Uri("http://localhost:" + hbs.OwnNodeDummy.Port + "/update"));
                hostUpdateService.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Main():" + ex.Message);
                if (hostUpdateService != null)
                    hostUpdateService.Close();
            }
        }
        //-------------------------------------------------------------------------------------------------------------------
        //EventHandler
        private static void HandleStateEvent(object sender, Update.CustomEventArgs e) {
            //change State from OwnNode
            hbs.OwnNodeList.GetNode(hbs.OwnNodeIndex).State = e.nodeStates;
        }

        private static void HandleCloseEvent(object sender, Update.CustomEventArgs e)
        {
            Uri uri = new Uri(@"http://" + hbs.OwnNodeDummy.IP + ":" + hbs.OwnNodeDummy.Port + @"/" + e.Message);
            ServiceHost host = userServiceHosts.Find(x => x.BaseAddresses.First() == uri);
            if (host != null)
            {
                host.Close();
                //remove Service from List
                Service service = hbs.OwnNodeList.GetNode(hbs.OwnNodeIndex).Services.Find(x => x.Uri == host.BaseAddresses.First());
                hbs.OwnNodeList.GetNode(hbs.OwnNodeIndex).RemoveService(service);
                hbs.OwnNodeList.SetNewTimestamp();
                userServiceHosts.Remove(host);
            }            
        }

        private static void HandleStartEvent(object sender, Update.CustomEventArgs e)
        {
            StartNewService(e.Message);
        }
        //-----------------------------------------------------------------------------------------------------------------------
        private static void StartNewService(string fileName)
        {
            //Load Dll
            Assembly assembly = Assembly.Load(File.ReadAllBytes(Path.Combine(UploadFolder, fileName)));
            Type contractType = assembly.GetTypes()[0];
            Type serviceType = assembly.GetTypes()[1];

            //Create ServiceHost
            ServiceHost newServiceHost = new ServiceHost(serviceType, new Uri(@"http://" + hbs.OwnNodeDummy.IP + ":" + hbs.OwnNodeDummy.Port + @"/" + fileName));
            //Add Behavior to ServiceHost to serve Metadata for the Clients
            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            newServiceHost.Description.Behaviors.Add(smb);
            //Add ServiceEndpoint and start Host
            newServiceHost.AddServiceEndpoint(contractType,
                new BasicHttpBinding(),
                "");
            newServiceHost.Open();

            Service service = new Service();
            service.Name = fileName;
            service.Uri = newServiceHost.BaseAddresses.First();
            //Add ServiceMethodParameters to Service
            ParameterInfo[] parameterInfos = contractType.GetMethods()[0].GetParameters();
            for (int i = 0; i < parameterInfos.Length; i++)
            {
                Parameter parameter = new Parameter();
                parameter.Name = parameterInfos[i].Name;
                parameter.Type = parameterInfos[i].ParameterType;
                service.AddParameter(parameter);
            }
            service.ReturnType = serviceType.GetMethods()[0].ReturnType;

            //add Servicehost to locallist
            userServiceHosts.Add(newServiceHost);
            //add Service to Nodelist
            hbs.OwnNodeList.GetNode(hbs.OwnNodeIndex).AddService(service);
            hbs.OwnNodeList.SetNewTimestamp();
        }

        //start new service 
        private static void StartAllUserServices()
        {
            //get dlls            
            DirectoryInfo d = new DirectoryInfo(UploadFolder);
            FileInfo[] files = d.GetFiles("*.dll");

            //create new service for every dll
            foreach (FileInfo file in files)
            {
                StartNewService(file.Name);
            }
        }

        private static Node SetupNode()
        {
            bool inputLoobStop = false;


            // Get ip numbers of this machine
            List<string[]> ipInterfaceList = GetIpAddressWithInterfaceName();
            for (int i = 0; i < ipInterfaceList.Count; i++)
            {
                Console.WriteLine("{0} ) {1}\n    {2}", i, ipInterfaceList[i][1], ipInterfaceList[i][0]);
            }

            // Select ip
            Console.WriteLine("Insert the number of the ip interface that is to use");
            inputLoobStop = false;
            int ipNumb = -1;
            while (!inputLoobStop)
            {
                bool couldParse = Int32.TryParse(Console.ReadLine(), out ipNumb);
                if (couldParse && ipNumb >= 0 && ipNumb < ipInterfaceList.Count)
                {
                    inputLoobStop = true;
                }
                else
                {
                    Console.WriteLine("Wrong input. Enter again:");
                }
            }

            IPAddress tempIP;
            IPAddress.TryParse(ipInterfaceList[ipNumb][0], out tempIP);
            hbs.OwnNodeDummy.IP = tempIP;

            // Insert port number
            Console.WriteLine("Enter port number to listen:");
            inputLoobStop = false;
            int tempPort = -1;
            while (!inputLoobStop)
            {
                bool couldParse = Int32.TryParse(Console.ReadLine(), out tempPort);
                if (couldParse && tempPort >= 0 && tempPort <= 65535)
                {
                    inputLoobStop = true;
                }
                else
                {
                    Console.WriteLine("Wrong input. Enter again:");
                }
            }
            hbs.OwnNodeDummy.Port = tempPort;

            // Host part
            try
            {
                heartbeatHost = new ServiceHost(typeof(HeartbeatService));
                heartbeatHost.AddServiceEndpoint(typeof(IHeartbeatService),
                    new BasicHttpBinding(),
                    new Uri("http://" + hbs.OwnNodeDummy.IP + ":" + hbs.OwnNodeDummy.Port + "/heartbeat"));
                heartbeatHost.Open();
                Console.WriteLine("Listening on Port " + hbs.OwnNodeDummy.Port);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SetupNode(): " + ex.Message);
                if (heartbeatHost != null)
                    heartbeatHost.Close();
            }

            // Get Node from existing system to get List
            bool stop = false;
            Node nodeToConnect = null;
            while (!stop)
            {
                Console.WriteLine("First node in system? (y / n)");
                string userInput = Console.ReadLine();
                if (userInput.ToLower() == "y")
                {
                    nodeToConnect = null;
                    stop = true;
                }
                else if (userInput.ToLower() == "n")
                {
                    Console.WriteLine("Insert ip of node to connect:");
                    string ipToConnect = "";
                    inputLoobStop = false;
                    while (!inputLoobStop)
                    {
                        ipToConnect = Console.ReadLine();
                        if (IsValidIpAdress(ipToConnect))
                        {
                            inputLoobStop = true;
                        }
                        else
                        {
                            Console.WriteLine("Wrong input. Enter again:");
                        }
                    }
                    Console.WriteLine("Insert port of node to connect:");
                    inputLoobStop = false;
                    tempPort = -1;
                    while (!inputLoobStop)
                    {
                        bool couldParse = Int32.TryParse(Console.ReadLine(), out tempPort);
                        if (couldParse && tempPort >= 0 && tempPort <= 65535)
                        {
                            inputLoobStop = true;
                        }
                        else
                        {
                            Console.WriteLine("Wrong input. Enter again:");
                        }
                    }
                    nodeToConnect = new Node(ipToConnect, tempPort);
                    stop = true;
                }
            }

            return nodeToConnect;
        }

        private static bool IsValidIpAdress(string ip)
        {
            try
            {
                IPAddress.Parse(ip);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("IsValidIpAdress(): " + e.Message);
                return false;
            }

        }

        private static List<string[]> GetIpAddressWithInterfaceName()
        {
            List<string[]> ipInterfaceList = new List<string[]>();

            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    ipInterfaceList.Add(new string[] { "", ni.Name });
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            ipInterfaceList[ipInterfaceList.Count - 1][0] = ip.Address.ToString();
                        }
                    }
                }
            }

            return ipInterfaceList;
        }      
        
        private static void StartRouter()
        {
            // Host part
            try
            {
                routingHost = new ServiceHost(typeof(RoutingService));
                routingHost.AddServiceEndpoint(typeof(IRoutingService),
                    new BasicHttpBinding(),
                    new Uri("http://localhost:" + hbs.OwnNodeDummy.Port + "/clientRouter"));
                routingHost.Open();
                //Console.WriteLine("Listening on Port " + hbs.OwnNodeDummy.Port);
            }
            catch (Exception ex)
            {
                Console.WriteLine("StartRouter():" + ex);
                if (routingHost != null)
                    routingHost.Close();
            }
        }
    }
}
