﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VS_Models;
using VS_Node_Heartbeat;

namespace VS_Node
{
    class Heartbeat
    {
        public Heartbeat()
        {
        }

        bool runningSymbolFlip = false;
        HeartbeatSingleton hbs = HeartbeatSingleton.Instance;

        public void StartHeartbeat(Node nodeToConnect, int heatbeatIntervall)
        {
            // Get nodeList form other node if not first.
            if (nodeToConnect == null)
            {
                hbs.OwnNodeList.AddNode(hbs.OwnNodeDummy, true);
                hbs.OwnNodeIndex = 0;
                hbs.FindeAndSetFollowingNode();
            }
            else
            {
                EndpointAddress ep = new EndpointAddress("http://" + nodeToConnect.IP + ":" + nodeToConnect.Port + "/heartbeat");
                ChannelFactory<IHeartbeatService> cFac = new ChannelFactory<IHeartbeatService>(new BasicHttpBinding(), ep);
                IHeartbeatService proxy = null;
                try
                {
                    proxy = cFac.CreateChannel();
                    hbs.OwnNodeList = proxy.GetNodeList();
                    hbs.OwnNodeList.AddNode(hbs.OwnNodeDummy, true);
                    hbs.OwnNodeIndex = hbs.OwnNodeList.Count() - 1;
                    hbs.FindeAndSetFollowingNode();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\n\n\nCouldn't reach " + nodeToConnect.IP + ":" + nodeToConnect.Port + ".\nNode will be closed. Restart Node to try again.");
                    Console.ReadLine();
                    System.Environment.Exit(1);
                }


            }

            ThreadStart listSendRef = new ThreadStart(HeartbeatThreadFunction);
            Thread HeartbeatThread = new Thread(listSendRef);
            HeartbeatThread.Start();

            void HeartbeatThreadFunction()
            {
                Console.WriteLine("Started Heartbeat ♥");
                try
                {
                    while (true)
                    {
                        EndpointAddress ep = new EndpointAddress("http://" + hbs.FollowingNode.IP + ":" + hbs.FollowingNode.Port + "/heartbeat");
                        ChannelFactory<IHeartbeatService> cFac = new ChannelFactory<IHeartbeatService>(new BasicHttpBinding(), ep);
                        IHeartbeatService proxy = null;
                        try
                        {
                            // Try to send List
                            proxy = cFac.CreateChannel();
                            proxy.ReceiveNodeList(hbs.OwnNodeList);

                            ShowHeartbeat();

                            
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine(ex);
                            // If node is offline, find next online node
                            if (!hbs.FollowingNode.IsSameNode(hbs.OwnNodeDummy))
                            {
                                hbs.OwnNodeList.SetNodeOffline(hbs.FollowingNodeIndex, true);
                            }
                            bool stop = false;
                            // Start with following node of the own following node ( ownNode(ONLINE) -> followingNode(OFFLINE) -> followingFollowingNode(?)
                            for (int i = hbs.FollowingNodeIndex + 1; !stop; i++)
                            {
                                // If over end of list, jump to start of list
                                i = i >= hbs.OwnNodeList.Count() ? 0 : i;

                                if (TryToPingNode(hbs.OwnNodeList.GetNode(i)))
                                {
                                    // If node can be reached
                                    hbs.FindeAndSetFollowingNode();
                                    stop = true;
                                }
                                else
                                {
                                    // If node can't be reached and is not same node, set offline
                                    if (!hbs.OwnNodeList.GetNode(i).IsSameNode(hbs.OwnNodeDummy))
                                    {
                                        hbs.OwnNodeList.SetNodeOffline(i, true);
                                    }
                                }
                            }
                            Console.WriteLine("\n\n");
                            //Console.WriteLine(ex);
                        }
                        Thread.Sleep(heatbeatIntervall);
                    }
                }
                catch (ThreadAbortException e)
                {
                    Console.WriteLine("Thread Abort Exception");
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    Console.WriteLine("Couldn't catch the Thread Exception");
                }
            }
        }

        private bool TryToPingNode(Node node)
        {
            EndpointAddress ep = new EndpointAddress("http://" + node.IP + ":" + node.Port + "/heartbeat");
            ChannelFactory<IHeartbeatService> cFac = new ChannelFactory<IHeartbeatService>(new BasicHttpBinding(), ep);
            IHeartbeatService proxy = null;
            try
            {
                Console.WriteLine(">>> try to ping: {0}:{1}", node.IP, node.Port);
                proxy = cFac.CreateChannel();
                proxy.ReturnPing();
                return true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex);
                return false;
            }
        }

        private void ShowHeartbeat()
        {
            Console.Clear();
            Console.WriteLine("List of nodes in system:\n");
            Console.WriteLine("---+----------------------------------------");
            for (int i = 0; i < hbs.OwnNodeList.Count(); i++)
            {
                Console.WriteLine("{0}. | {1}:{2} - {3} {4}\n---+----------------------------------------",
                    i,
                    hbs.OwnNodeList.GetNode(i).IP,
                    hbs.OwnNodeList.GetNode(i).Port,
                    hbs.OwnNodeList.GetNode(i).State,
                    hbs.OwnNodeList.GetNode(i).State == Node.NodeStates.Offline ? "dies in " + ((hbs.TimeToLive - (DateTimeOffset.Now.ToUnixTimeMilliseconds() - hbs.OwnNodeList.GetNode(i).StateChangeTimestemp)) / 1000) + " sec" : "");
            }
            Console.WriteLine("\n\nList Timestamp: {0}\n", hbs.OwnNodeList.Timestamp);
            Console.WriteLine("This Node: {0}:{1}", hbs.OwnNodeList.GetNode(hbs.OwnNodeIndex).IP, hbs.OwnNodeList.GetNode(hbs.OwnNodeIndex).Port);
            Console.WriteLine("Following Node: {0}:{1}", hbs.FollowingNode.IP, hbs.FollowingNode.Port);
            if (runningSymbolFlip)
            {
                /*
                Console.WriteLine("  ┌─┐");
                Console.WriteLine("  │ │");
                Console.WriteLine("  │ │");
                Console.WriteLine("  └─┘");*/
                Console.WriteLine(" .:::.   .:::.");
                Console.WriteLine(":::::::.:::::::");
                Console.WriteLine(":::::::::::::::");
                Console.WriteLine("':::::::::::::'");
                Console.WriteLine("  ':::::::::'");
                Console.WriteLine("    ':::::'");
                Console.WriteLine("      ':'");
                runningSymbolFlip = !runningSymbolFlip;
            }
            else
            {
                /*Console.WriteLine("");
                Console.WriteLine("┌──────┐");
                Console.WriteLine("└──────┘");
                Console.WriteLine("");*/
                Console.WriteLine(" .:::.   .:::.");
                Console.WriteLine(":     :.:     :");
                Console.WriteLine(":             :");
                Console.WriteLine("':           :'");
                Console.WriteLine("  ':       :'");
                Console.WriteLine("    ':   :'");
                Console.WriteLine("      ':'");
                runningSymbolFlip = !runningSymbolFlip;
            }
        }
    }
}
